{{- if eq .Values.common.environmentType "auth" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: WCSV9
    chart: {{ .Chart.Name }}-{{ .Chart.Version}}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
  name: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}{{ .Values.common.environmentType }}{{.Values.searchAppMaster.name}}
  namespace: {{ .Release.Namespace }}
spec:
  selector: 
    matchLabels:
     app: WCSV9
     chart: {{ .Chart.Name }}
     release: {{ .Release.Name }}
     heritage: {{ .Release.Service }}
     component: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}{{ .Values.common.environmentType }}{{ .Values.searchAppMaster.name }}
     group: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}{{ .Values.common.environmentType }}
  replicas: 1
  template:
    metadata:
      annotations:
        productName: "WebSphere Commerce"
        productID:  {{ .Values.common.productionID | quote }} 
        productVersion: {{ .Values.common.productVersion | quote }}
      labels:
        app: WCSV9
        chart: {{ .Chart.Name }}
        release: {{ .Release.Name }}
        heritage: {{ .Release.Service }}
        component: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}{{ .Values.common.environmentType }}{{ .Values.searchAppMaster.name }}
        group: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}{{ .Values.common.environmentType }}
    spec:
      affinity:
        nodeAffinity:
           {{- if .Values.searchAppMaster.nodeLabel }}
           requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
              - matchExpressions:
                - key: wc-node-select-flag
                  operator: In
                  values: 
                  - {{ .Values.searchAppMaster.nodeLabel }}
           {{- else }}
           requiredDuringSchedulingIgnoredDuringExecution:
           {{- include "nodeAffinityRequiredDuringScheduling" . | indent 8 }}
           {{- end }}   
           preferredDuringSchedulingIgnoredDuringExecution:
           {{- include "nodeAffinityPreferredDuringScheduling" . | indent 8 }}
      hostNetwork: false
      hostPID: false
      hostIPC: false
      serviceAccountName: {{ .Values.common.serviceAccountName | default "default" }}
      initContainers:
      - name: search-dependence-check
        image: "{{ .Values.common.imageRepo }}{{ .Values.supportC.image }}:{{ .Values.supportC.tag }}"
        {{- if .Values.common.imagePullPolicy }}
        imagePullPolicy: {{ .Values.common.imagePullPolicy }}
        {{- end }}
        args: ['depcheck', '-component','search','-tenant',{{ .Values.common.tenant }},'-env',{{ .Values.common.environmentName}},'-envtype',{{ .Values.common.environmentType }},'-namespace',{{ .Release.Namespace }},'-interval_time','20','-expect_during_time','600','-timeout','5','-spiuser_pwd_encrypte',{{ .Values.common.spiUserPwdBase64 }}]
      volumes:
        - name: logs-sharing
          emptyDir: {}
      {{- if .Values.searchAppMaster.persistenVolumeClaim }}
        - name: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}-search-master-index-volume
          persistentVolumeClaim:
            claimname: {{ .Values.searchAppMaster.persistenVolumeClaim }}
      {{- else }}
        {{- if .Values.persistenVolumeClaim.autoCreate }}
        - name: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}-search-master-index-volume
          persistentVolumeClaim:
            claimname: {{ .Values.common.tenant }}{{ .Values.common.environmentName}}-search-master-volume
        {{- end }}
      {{- end }}
      {{- if .Values.common.bindingConfigMap }}
        - name: config-volume
          configMap:
            name: {{ .Values.common.bindingConfigMap }} 
      {{- end }}
      {{- if and .Values.fileBeat.enable .Values.searchAppMaster.fileBeatConfigMap }}
        - name: filebeat-config-search-app-master
          configMap:
            name: {{ .Values.searchAppMaster.fileBeatConfigMap }}
      {{- end }}
      {{- if .Values.common.imagePullSecrets }}
      imagePullSecrets:
        - name: {{ .Values.common.imagePullSecrets }}
      {{- end }}
      containers:
      - name: search-app-master
        image: "{{ .Values.common.imageRepo }}{{.Values.searchAppMaster.image}}:{{ .Values.searchAppMaster.tag }}"
        {{- if .Values.common.imagePullPolicy }}
        imagePullPolicy: {{ .Values.common.imagePullPolicy }}
        {{- end }}
        resources:
{{ toYaml .Values.searchAppMaster.resources | indent 12 }}
        env:
        - name: "LICENSE"
          value: {{ .Values.license | quote }}
        {{- if .Values.common.commerceVersion }}
        {{- if (or (eq .Values.common.commerceVersion "9.0.0.1") (eq .Values.common.commerceVersion "9.0.0.0")) }}
          {{- if .Values.common.overridePreconfig }}
        - name: "OVERRIDE_PRECONFIG"
          value: {{ .Values.common.overridePreconfig | quote }}
          {{- end }}
        {{- end }}
        {{- else }}
        - name: "CONFIGURE_MODE"
          value: {{ .Values.common.configureMode | quote }}  
        {{- end }}
        - name: "ENVIRONMENT"
          value: {{ .Values.common.environmentName | quote }}
        - name: "TENANT"
          value: {{.Values.common.tenant | quote}}
        - name: "ENVTYPE"
          value: {{ .Values.common.environmentType | quote }}
        - name: "DOMAIN_NAME"
          value: "{{ .Release.Namespace }}.svc.cluster.local"
        - name: "DBTYPE"
          value: {{ .Values.common.dbType | quote }}
        - name: "VAULT_CA"
          value: {{ .Values.vaultCA.enable | quote }}
        - name: "WORKAREA"
          value: "/search"
        - name: "SOLR_MASTER"
          value: "true"
        - name: "SOLR_SLAVE"
          value: "false"
        - name: "SPIUSER_NAME"
          value: {{ .Values.common.spiUserName | quote }}
        - name: "SPIUSER_PWD"
          value: {{ .Values.common.spiUserPwdAes | quote }}
        {{- if (or (eq .Values.common.configureMode "Vault") .Values.vaultCA.enable) }}
        - name: "VAULT_URL"
          value: {{ .Values.common.vaultUrl | quote }}
        {{- if .Values.common.vaultTokenSecret }}
        - name: "VAULT_TOKEN"
          valueFrom:
            secretKeyRef:
              name: {{ .Values.common.vaultTokenSecret }}
              key: VAULT_TOKEN
        {{- end }}
        {{- end }}
{{- if .Values.searchAppMaster.envParameters }}
     {{- range $key, $value := .Values.searchAppMaster.envParameters }}
        - name: {{ $key | quote}}
          value: {{ $value | quote }}
     {{- end }} 
{{- end }}        
        ports:
        - containerPort: 3737
          name: port3737
        - containerPort: 3738
          name: port3738
          protocol: TCP
        readinessProbe:
          httpGet:
            path: /search/admin/resources/health/status?type=container
            port: 3737
            httpHeaders:
            - name: Authorization
              value: Basic {{ .Values.common.spiUserPwdBase64 }}
          initialDelaySeconds: 5
          periodSeconds: 5
        livenessProbe:
          tcpSocket:
            port: 3737
          initialDelaySeconds: 600
          timeoutSeconds: 300
        securityContext:
            allowPrivilegeEscalation: true
            readOnlyRootFilesystem: false
            runAsNonRoot: false
            runAsUser: 0
            privileged: false
            capabilities:
              drop:
              - all
              add:
              - CHOWN
              - DAC_OVERRIDE
              - FOWNER
              - FSETID
              - KILL
              - SETGID
              - SETUID
              - SETPCAP
              - NET_BIND_SERVICE
              - NET_RAW
              - SYS_CHROOT
              - MKNOD
              - AUDIT_WRITE
              - SETFCAP
        volumeMounts:
          - name: logs-sharing
            mountPath: /opt/WebSphere/Liberty/usr/servers/default/logs/container
      {{- if or .Values.persistenVolumeClaim.autoCreate .Values.searchAppMaster.persistenVolumeClaim }}
          - name:  {{ .Values.common.tenant }}{{ .Values.common.environmentName}}-search-master-index-volume
            mountPath: /search
      {{- end }}
      {{- if .Values.common.bindingConfigMap }}
          - name: config-volume
            mountPath: /SETUP/ext-config/config.properties
            subPath: config.properties
            readOnly: false     
      {{- end }} 
{{- if .Values.fileBeat.enable }}
      - name: filebeat
        image: "{{ .Values.common.imageRepo }}{{ .Values.fileBeat.image }}:{{ .Values.fileBeat.tag }}"
        {{- if .Values.common.imagePullPolicy }}
        imagePullPolicy: {{ .Values.common.imagePullPolicy }}
        {{- end }}
        resources:
{{ toYaml .Values.fileBeat.resources | indent 12 }}
        args: ["-indexName", "{{ .Values.common.tenant }}-{{ .Values.common.environmentName}}-{{ .Values.common.environmentType }}", "-targetELK", "{{ .Values.fileBeat.elkServer }}", "-componentType","search-app-master"]
        securityContext:
            allowPrivilegeEscalation: true
            readOnlyRootFilesystem: false
            runAsNonRoot: false
            runAsUser: 0
            privileged: false
            capabilities:
              drop:
              - all
              add:
              - CHOWN
              - DAC_OVERRIDE
              - FOWNER
              - FSETID
              - KILL
              - SETGID
              - SETUID
              - SETPCAP
              - NET_BIND_SERVICE
              - NET_RAW
              - SYS_CHROOT
              - MKNOD
              - AUDIT_WRITE
              - SETFCAP
        volumeMounts:
        - name: logs-sharing
          mountPath: /log
{{- if .Values.searchAppMaster.fileBeatConfigMap }}
        - name: filebeat-config-search-app-master
          mountPath: /etc/filebeat
{{- end }}
{{- end }}


{{- end -}}
