# IBM WebSphere Commerce

## Introduction
WebSphere Commerce is a single, unified e-commerce platform that offers the ability to do business directly with consumers (B2C) or directly with businesses (B2B). It is also a customizable, scalable, distributed, and high availability solution that is built to use open standards. WebSphere Commerce uses cloud friendly technology to make deployment and operation both easy and efficient. It provides easy-to-use tools for business users to centrally manage a cross-channel strategy. Business users can create and manage precision marketing campaigns, promotions, catalog, and merchandising across all sales channels. Business users can also use AI enabled content management capabilities.

WebSphere Commerce Version 9 is full Docker-based release. For more information, see: [WebSphere Commerce Version 9](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/landing/wc_welcome.htm).

A complete WebSphere Commerce V9 environment compose with Auth environment and Live environment. Auth environment for site administrator. Live environment for real user access. See [WebSphere Commerce V9 Full Topology](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.install.doc/refs/riginfrastructure.htm)

Vault-Consul is a mandatory component that is used by default Certification Agent to automatically issue certifications. It is also used by the Configuration Center to store environment-related data.


## Chart Details
* Supports the deployment of WebSphere Commerce Version 9 on Kubernetes.
* Supports the deployment of multiple WebSphere Commerce Authoring and Live environments.
* By default, quick deployment with the sample Db2 Docker image is enabled, requiring only minimum configuration.
* With quick deployment, a binding configmap is auto-generated with the <Tenant><Environment><EnvType>-config.properties file. If quick deployment is disabled, ensure that the valid configmap is created, or leave the “Binding ConfigMap” field empty. Refer to the custom binding configmap in the "Self Custom Configuration Deploy" section.
* Supports the deployment of complex WebSphere Commerce environments with self-defined configurations.
* Tenant / Environment / Environment Type / NameSpace are used to identify unique environments. As a best-practice, name your release and related resources with this naming pattern.

The following is an example of how to use Tenant / Environment / Environment Type to naming resource:

```
Tenant: demo
Environment: qa
Environment Type: auth

Name this with a release name, such as demoqaauth.
```
## Prerequisites
1. IBM Cloud Private version 3.1+ is installed.
2. Vault/Consul is deployed and initialized for WebSphere Commerce. Use the Helm Chart: ibm-websphere-commerce-vaultconsul. If Quick Deploy Mode is enabled, VaultConsul must be deployed in the same namespace with WebSphere Commerce Vesion 9.
3. The default Docker images for WebSphere Commerce are loaded to an appropriate Docker Image Repository.

   If IBM Passport Advantage is loaded to IBM Cloud Private, the Docker Image is automatically loaded to ICP’s default Docker Registry in the “commerce” namespace. See [How to load Commerce V9 Cloud Pak to IBM Cloud Private](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.install.doc/concepts/cig_wcicpover.htm)

   Docker Images  | Tag |
   --------  | -----|
   commerce/ts-app | 9.0.1.5 |
   commerce/ts-web| 9.0.1.5 |
   commerce/search-app | 9.0.1.5 |
   commerce/crs-app | 9.0.1.5 |
   commerce/ts-db | 9.0.1.5 |
   commerce/xc-app | 9.0.1.5 |
   commerce/ts-utils | 9.0.1.5 |
   commerce/supportcontainer|1.0.0|
   commerce/filebeat|1.0.1|

4. WebSphere Commerce default Docker Image can be pulled on all of Kubernetes worker nods.
5. Optional: WebSphere Commerce default Sample Db2 Docker image is not supported on Centos7 with the overlay storage driver. If you are using Centos7, change the default storage driver to devicemapper. For more information, see [How to change storage drvier](https://docs.docker.com/storage/storagedriver/device-mapper-driver/#configure-loop-lvm-mode-for-testing).
6. Optional: If use non-docker database, the database must be prepared. see [How to setup Commerce V9 database](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.install.doc/tasks/tig_prepare_db.htm)
7. Role Based Access Control(RBAC) must be created first and only once on the target namespace if you deploy WebSphere Commerce Version 9 environment on a non-default namespace.
   Perform this with the Cluster Admin.

 The following is an example to create RBAC for default service account on target namespace:

```
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: commerce-deploy-support-<namespace>
  namespace: <namespace>
rules:
- apiGroups: [""]
  resources: ["secrets"]
  verbs: ["get", "watch", "list","create","delete","patch","update"]
- apiGroups: [""]
  resources: ["persistentvolumeclaims"]
  verbs: ["get", "watch", "list","create","delete","patch","update"]
- apiGroups: [""]
  resources: ["pods","pods/log"]
  verbs: ["get", "watch", "list","create","delete","patch","update"]
- apiGroups: [""]
  resources: ["configmaps"]
  verbs: ["get", "watch", "list","create","delete","patch","update"]
---

kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: commerce-deploy-support-<namespace>
  namespace: <namespace>
subjects:
- kind: ServiceAccount
  name: default
  namespace: <namespace>
roleRef:
  kind: Role
  name: commerce-deploy-support-<namespace>
  apiGroup: rbac.authorization.k8s.io
```

8. Optional: If use Vault as configuration center to store configuration data, instead of Quick Deploy Mode, recommend to assign special vault token for different environment for security. Vault token must be stored as secret object and specify the vault token secret name when deploy Commerce V9 through UI. See [How to manage Vault Token](https://www.vaultproject.io/docs/concepts/tokens.html). See How to prepare data on vault in section `Custom Read All Data From Vault` below.

```
# Replace xxxx with real vault_token value
export vault_token=xxxx
encrypted_vault_token=`echo -n "${vault_token}" | base64`

cat << EOF > ./vault-token-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: <SecretName>
  namespace: <NameSpace>
type: Opaque
data:
  VAULT_TOKEN: ${encrypted_vault_token}
EOF

kubectl create -f ./vault-token-secret.yaml
```

> Note: Please correct the SecretName and NameSpace

9. Optional: If you change the default Docker Repository, create "Image Policies" in Manage -> Resource Security -> Image Policies.

10. Optional: The WebSphere Commerce Version 9 default Docker image is uploaded to the commerce namespace. If you deploy WebSphere Commerce Version 9 on a different namespace, create imagePullSecrets in target namespace with admin user, and specify the imagePullSecrets name in common.imagePullSecrets. Fore more information, see [how to create imagePullSecrets](https://www.ibm.com/support/knowledgecenter/SSBS6K_3.1.0/manage_images/imagepullsecret.html).

11. Optional: As default, if not specify nodeLabel for each service. Helm will add architecture as requiredDuringSchedulingIgnoredDuringExecution rule for nodeAffinity. If you want custom requiredDuringSchedulingIgnoredDuringExecution of nodeAffinity, please add node label 'wc-node-select-flag' for worker, then special value in nodeLabel field for each service.


### PodSecurityPolicy Requirements

This chart requires a PodSecurityPolicy to be bound to the target namespace prior to installation.  Choose either a predefined PodSecurityPolicy or have your cluster administrator setup a custom PodSecurityPolicy for you:
* Predefined PodSecurityPolicy name: [`ibm-privileged-psp`](https://ibm.biz/cpkspec-psp)
* Custom PodSecurityPolicy definition:

> Note: This PodSecurityPolicy only needs to be created once. If it already exist, skip this step.

```
apiVersion: extensions/v1beta1
kind: PodSecurityPolicy
metadata:
  name: commerce-psp
spec:
  hostIPC: false
  allowPrivilegeEscalation: true
  readOnlyRootFilesystem: false
  allowedCapabilities:
  - CHOWN
  - DAC_OVERRIDE
  - FOWNER
  - FSETID
  - KILL
  - SETGID
  - SETUID
  - SETPCAP
  - NET_BIND_SERVICE
  - NET_RAW
  - SYS_CHROOT
  - MKNOD
  - AUDIT_WRITE
  - SETFCAP
  - SYS_RESOURCE
  - IPC_OWNER
  - SYS_NICE
  seLinux:
    rule: RunAsAny
  supplementalGroups:
    rule: RunAsAny
  runAsUser:
    rule: RunAsAny
  fsGroup:
    rule: RunAsAny
  volumes:
  - configMap
  - emptyDir
  - persistentVolumeClaim
  - secret
  forbiddenSysctls:
  - '*'
```

* Custom ClusterRole for the custom PodSecurityPolicy:

```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: commerce-psp-clusterrole
rules:
- apiGroups:
  - extensions
  resourceNames:
  - commerce-psp
  resources:
  - podsecuritypolicies
  verbs:
  - use

```

The cluster admin can either paste the above PSP and ClusterRole definitions into the create resource screen in the UI or run the following two commands:

- `kubectl create -f <PSP yaml file>`
- `kubectl create clusterrole commerce-psp-clusterrole --verb=use --resource=podsecuritypolicy --resource-name=commerce-psp`

In ICP 3.1, you also need to create the RoleBinding:

- `kubectl create rolebinding commerce-psp-rolebinding --clusterrole=commerce-psp-clusterrole --serviceaccount=<namespace>:default --namespace=<namespace>`

In ICP 3.1.1+, you need to create the RoleBinding:

- `kubectl create rolebinding ibm-psp-rolebinding --clusterrole=ibm-privileged-clusterrole --serviceaccount=<namespace>:default --namespace=<namespace>`

## Resources Required
By default, when you use the Helm Chart to deploy WebSphere Commerce Version 9, you start with the following number of Pods and required resources:

Component  | Replica | Request CPU | Limit CPU | Request Memory | Limit Memory
--------  | -----| -------------| -------------| -------------| -------------
ts-app | 1 | 500m |  2 | 2048Mi | 4096Mi
search-app-master | 1 | 500m |  2 | 2048Mi | 4096Mi
search-app-repeater ( live ) | 1 | 500m |  2 | 2048Mi | 4096Mi
search-app-slave ( live ) | 1 | 500m |  2 | 2048Mi | 4096Mi
crs-app | 1 | 500m |  2 | 2048Mi | 4096Mi
xc-app | 1 | 500m |  2 | 2048Mi | 4096Mi
ts-db | 1 | 500m |  2 | 2048Mi | 4096Mi
ts-web| 1 | 500m |  2 | 2048Mi | 4096Mi


Note: Ensure that you have sufficient resources available on your worker nodes to support the WebSphere Commerce Version 9 deployment.

## Configuration

Tip: You can use the values-mini-custom.yaml as reference for minimize change.

The following tables lists the configurable parameters of the ibm-websphere-commerce chart and their default values.

| Parameter                  | Description                                     | Default                                                    |
| -----------------------    | ---------------------------------------------   | ---------------------------------------------------------- |
| `license`             | IBM WebSphere Commerce V9 license accept             | `not_accepted`                                                        |
| `common.vaultTokenSecret`  | Kubernetes secret object for vault token        | `nil`                                                    |
| `common.dbType`         | database type           | `db2`    |
| `common.tenant`                |   tenant name                          | `demo`                                                   |
| `common.environmentName`       |   environment name | `qa`                                                |
| `common.environmentType`     |   environment type \[auth\|live\]            | `auth`                                                       |
| `common.imageRepo`      |   docker image registry             | `mycluster.icp:8500/`
| `common.spiUserName`          |  spiuser name for Commerce                     | `spiuser`                                                    |
| `common.spiUserPwdAes`        |  spiuser name password by encrypted with AES by wc_encrypt.sh, default plain text password is passw0rd| `eNdqdvMAUGRUbiuqadvrQfMELjNScudSp5CBWQ8L6aw`                                                    |
| `common.spiUserPwdBase64`        | Specify spiuser name password by encrypted with base64, default plain text password is passw0rd| `c3BpdXNlcjpwYXNzdzByZA==`
| `common.vaultUrl`        |  vault url| `http://vault-consul:8200/v1`
| `common.externalDomian`        | External Domain use to specify the service external domain name| `.ibm.com`
| `common.bindingConfigMap`        | ConfigMap name which mount into each default container to expose OS variables| `demoqaauth-config.properties`
| `common.ingressAnnoation`        |  custom ingress annotation | `ingress.kubernetes.io`
| `common.configureMode`        |  default container config mode (not support change) | `Vault`
| `common.imagePullSecrets`        |  image pull secretes if deploy on non-commerce namespace | `nil`
| `common.imagePullPolicy`        |  image pull policy \[IfNotPresent\|Always\] | `IfNotPresent`
| `common.serviceAccountName`        |  serviceAccount used for helm release  | `default`
| `ingressSecrete.autoCreate`        |  specify if need helm pre-install auto create ingress certification secret| `true`
| `ingressSecrete.replaceExist`        |  specify if need to force replace exist ingress certification secret when deploy | `true`
| `persistenVolumeClaim.autoCreate`        |  specify if need to helm auto create PVC | `false`
| `persistenVolumeClaim.storageClass`        |  persistentVolumClaim class name | `glusterfs`
| `persistenVolumeClaim.accessModes`        |  specify read and write permission on target PV  | `ReadWriteMany`
| `persistenVolumeClaim.storageSize`        |  specify persistent volume size  | `1Gi`
| `persistenVolumeClaim.force`        |  specify if need to force create PVC if the same name PVC exist  | `false`
| `persistenVolumeClaim.cleanPvc`        |  specify if need to remove PVC and PV when release be deleted  | `false`
| `createSampleConfig.enable`        |  Enable or Disable the Quick Deploy Mode  | `true`
| `createSampleConfig.dbHostName`        |  database server address for Quick Deploy Mode  | `demoqaauthdb`
| `createSampleConfig.dbName`        |  database instance name for Quick Deploy Mode  | `mall`
| `createSampleConfig.dbUser`        |  database instance user name for Quick Deploy Mode  | `wcs`
| `createSampleConfig.dbPass`        |  database instance user password for Quick Deploy Mode  | `wcs1`
| `createSampleConfig.dbPort`        |  database port for Quick Deploy Mode  | `50000`
| `createSampleConfig.dbaUser`        |  database admin user name for Quick Deploy Mode  | `db2inst1`
| `createSampleConfig.dbaPassEncrypt`        |  database admin user password which be encrypted by wc_encrypt.sh in utilities docker for Quick Deploy Mode  | `u4h5LS/vJeSzsCfnt6NGfGHYPWK2fRq0+djkmB/iNWU=`
| `createSampleConfig.dbPassEncrypt`        |  database instance user password which be encrypted by wc_encrypt.sh in utilities docker for Quick Deploy Mode  | `okFQawPB19Tkl1wKqQPDdAVtwz`
| `vaultCA.enable`        |  enable VaultCA configuration mode (not support to change)  | `true`
| `tsDb.enable`        |  enable deploy sample database container  | `true`
| `tsDb.image`        |  sample database docker image name  | `commerce/ts-db`
| `tsDb.tag`        |  sample database docker image tag  | `9.0.1.5`
| `tsDb.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `tsDb.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `tsDb.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `tsDb.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `tsDb.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution  | `nil`
| `tsApp.name`        |  transaction container name  | `ts-app`
| `tsApp.replica`     |  number of transaction replica  | `1`
| `tsApp.image`        |  transaction docker image name | `commerce/ts-app`
| `tsApp.tag`        |  transaction docker image tag | `9.0.1.5`
| `tsApp.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `tsApp.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `tsApp.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `tsApp.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `tsApp.merchantKey`        |  specify custom merchantKey for transaction container  | `eZrWIdOyaDv5FCOTK32Uni288jgIHDv/P9wxhzKmHdiGZ+n8WJ+Ah56uPbfZ9yJWtjQlGczlmr6OgvArFHCgZQ==`
| `tsApp.envParameters`        |  specify custom environment parameter with key-value format. Not support configure on ICP catalog UI  | `nil`
| `tsApp.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution  | `nil`
| `tsApp.fileBeatConfigMap`        |  configMap name for specify custom filebeat configuration in sidecar filebeat   | `nil`
| `searchAppMaster.name`        |  search master container name  | `search-app-master`
| `searchAppMaster.image`        |  search master docker image name | `commerce/search-app`
| `searchAppMaster.tag`        |  search master docker image tag | `9.0.1.5`
| `searchAppMaster.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `searchAppMaster.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `searchAppMaster.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `searchAppMaster.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `searchAppMaster.envParameters`        |  specify custom environment parameter with key-value format. Not support configure on ICP catalog UI  | `nil`
| `searchAppMaster.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution  | `nil`
| `searchAppMaster.persistenVolumeClaim`        |  sepcify exist PVC for search master  | `nil`
| `searchAppMaster.fileBeatConfigMap`        |  configMap name for specify custom filebeat configuration in sidecar filebeat   | `nil`
| `searchAppRepeater.name`        |  search repeater container name  | `search-app-repeater`
| `searchAppRepeater.image`        |  search repeater docker image name | `commerce/search-app`
| `searchAppRepeater.tag`        |  search repeater docker image tag | `9.0.1.5`
| `searchAppRepeater.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `searchAppRepeater.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `searchAppRepeater.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `searchAppRepeater.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `searchAppRepeater.envParameters`        |  specify custom environment parameter with key-value format. Not support configure on ICP catalog UI  | `nil`
| `searchAppRepeater.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution  | `nil`
| `searchAppRepeater.persistenVolumeClaim`        |  sepcify exist PVC for search repeater  | `nil`
| `searchAppRepeater.fileBeatConfigMap`        |  configMap name for specify custom filebeat configuration in sidecar filebeat   | `nil`
| `searchAppSlave.name`        |  search slave container name  | `search-app-slave`
| `searchAppSlave.replica`     |  number of search slave replica  | `1`
| `searchAppSlave.image`        |  search slave docker image name | `commerce/search-app`
| `searchAppSlave.tag`        |  search slave docker image tag | `9.0.1.5`
| `searchAppSlave.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `searchAppSlave.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `searchAppSlave.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `searchAppSlave.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `searchAppSlave.envParameters`        |  specify custom environment parameter with key-value format. Not support configure on ICP catalog UI  | `nil`
| `searchAppSlave.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution | `nil`
| `searchAppSlave.persistenVolumeClaim`        |  specify exist PVC for search repeater  | `nil`
| `searchAppSlave.fileBeatConfigMap`        |  configMap name for specify custom filebeat configuration in sidecar filebeat   | `nil`
| `tsWeb.name`        |  transaction web container name  | `ts-web`
| `tsWeb.replica`     |  number of transaction web replica  | `1`
| `tsWeb.image`        |   transaction web docker image name | `commerce/search-app`
| `tsWeb.tag`        |   transaction web docker image tag | `9.0.1.5`
| `tsWeb.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `tsWeb.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `tsWeb.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `tsWeb.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `tsWeb.envParameters`        |  specify custom environment parameter with key-value format. Not support configure on ICP catalog UI  | `nil`
| `tsWeb.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution  | `nil`
| `tsWeb.fileBeatConfigMap`        |  configMap name for specify custom filebeat configuration in sidecar filebeat   | `nil`
| `crsApp.name`        |  storefront container name  | `crs-app`
| `crsApp.replica`     |  number of storefront replica  | `1`
| `crsApp.image`        |   transaction web docker image name | `commerce/crs-app`
| `crsApp.tag`        |   transaction web docker image tag | `9.0.1.5`
| `crsApp.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `crsApp.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `crsApp.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `crsApp.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `crsApp.envParameters`        |  specify custom environment parameter with key-value format. Not support configure on ICP catalog UI  | `nil`
| `crsApp.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution | `nil`
| `crsApp.fileBeatConfigMap`        |  configMap name for specify custom filebeat configuration in sidecar filebeat   | `nil`
| `xcApp.name`        |  user extension container name  | `xcApp`
| `xcApp.replica`     |  number of user extension replica  | `1`
| `xcApp.image`        |   user extension docker image name | `commerce/xc-app`
| `xcApp.tag`        |   user extension docker image tag | `9.0.1.5`
| `xcApp.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `xcApp.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `xcApp.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `xcApp.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `xcApp.envParameters`        |  specify custom environment parameter with key-value format. Not support configure on ICP catalog UI  | `nil`
| `xcApp.nodeLabel`        |  specify value of node label 'wc-node-select-flag' to enable nodeAffinity with requiredDuringSchedulingIgnoredDuringExecution  | `nil`
| `xcApp.fileBeatConfigMap`        |  configMap name for specify custom filebeat configuration in sidecar filebeat   | `nil`
| `supportC.image`        |  supportcontainer docker image for initial container and helm pre-install / post-delete  | `commerce/supportcontainer`
| `supportC.tag`        |  supportcontainer docker tag  | `1.0.0`
| `fileBeat.enable`        |  specify if need start filebeat as sidecar with WebSphere Commerce default container  | `false`
| `fileBeat.image`        |  specify custom filebeat docker image   | `commerce/filebeat`
| `fileBeat.tag`        |  specify custom filebeat docker image tag  | `1.0.0`
| `fileBeat.resources.requests.memory`        |  request memory for scheduler pod   | `2048Mi`
| `fileBeat.resources.requests.cpu`        |  request cpu for scheduler pod   | `500m`
| `fileBeat.resources.limits.memory`        |  limit memory for pod running  | `4096Mi`
| `fileBeat.resources.limits.cpu`        |  limit memory for pod running  | `2`
| `fileBeat.elkServer`        |  specify target external ElasticSearch server  | `nil`
| `test.image`        |  test docker image for helm test  | `docker.io/centos:latest`

### Quick Deploy ##
With minimal configuration changes, you can use Quick Deploy to deploy a full topology of WebSphere Commerce Version 9. The Quick Deploy model enables "createSampleConfig" in values.yaml and deploys the sample Db2 Docker image.

#### Deploy Auth Environment ####
By default, Quick Deploy sets the tenant name as demo. For example,  "qa" is the environment name, and "auth" as the default environment type. The first time you run a quick deployment, complete the following:

1. Fill in the release name; for example, demoqaauth.
2. Select the target Namespace.
3. Optional: Specify "Image Pull Secrets" in the "Common Configuration for All Component Container" block for the non-commerce namespace if you use the ICP default Docker Repository.
3. Optional: Correct the Docker Repository if you don't use ICP default Docker Repository
4. Optional: Specify the Docker Image tag for each service if you don't use default tag.

#### Deploy Live Environment ####
If you want to deploy WebSphere Commerce Version 9 Live environment, complete the following:

1. Fill in release name; for example, demoqalive.
2. Select the Namespace, which is the same as your auth environment.
3. Optional: Correct the Docker Repository if you don't use the ICP default Docker Repository.
4. Optional: Specify "Image Pull Secrets" in the "Common Configuration for All Component Container" block for the non-commerce namespace if use the ICP default Docker Repository.
5. Update the "Environment Type" to "live" in the "Common Configuration for All Component Container" block.
6. Update the "Binding ConfigMap" to "demoqalive-config.properties" in the "Common Configuration for All Component Container" block.
7. Update the "Database HostName" to "demoqalivedb" in the "Quick Deploy Configuration" block.
8. Optional: Specify the Docker Image tag for each service if you don't want use default tag.

#### Custom Database Information ####
If you are connecting to an existing database, complete the following:


1. Update the database related information in the "Create Sample Config" block.
2. Disable "Enable Sample DB Container" in the "Database" block.

### Self Custom Configuration Deploy ##
If you want to deploy a more complex configuration, customize the default configuration values.

#### Custom Configuration In Kubernetes ConfigMap ####
WebSphere Commerce Version 9 supports mount config.properties under the path /SETUP/ext-config with key/value pairs to expose environment variables to the container for start-up configuration. The environment variables have higher priority than the data that is fetched from the Vault. 

When Quick Deploy Configuration is enabled, the default configmap is generated automatically with the data in "Quick Deploy Configuration" and named with <Tenant><Environment><EnvType>--config.properties.

If Quick Deploy Configuration is disabled, you can create the configmap with key/values pairs, then fill in the correct configmap name in field of "Binding ConfigMap". This way, you don't need to populate the data to Vault/Consul, but be aware that this is not a secure approach, and not recommended on production environments.

For more information, see [Environment Variables For WebSphere Commerce Version 9 Configuration](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.install.doc/refs/rigvaultmetadata.htm).

#### Custom Read All Data From Vault ####
With Quick Deploy, mandatory environment data is read from the pre-defined ConfigMap. If you want to set more
complex environment related data, disable Quick Deploy mode and populate sensive data to Vault

1. Disable "Enable Create Sample Config" in the "Create Sample Config" block.
2. Populate your environment related data to Vault. 
3. Create Vault Token secret object on target namespace ( The easy way is using the root token which can get from step2#, but it is insecurity. Recommend to create different read/write role token for different enviornment)
4. Correct Vault URL with a public address can be access from all namespace when do the deployment. As default Vault will expose port as NodePort. so the URL can be http://any-worker-ip:vault-node-port/v1. Check Vault Node Port on ICP Network Access --> Services


Run below steps on ICP master node to populate data on Vault:

1. Get the Vault Token.
```
# specify target namespace
export namespace=xxxx
export icp_master_ip=xxxx
export vault_pod=$(kubectl get po -n ${namespace} | grep vault.consul| awk '{print $1}')
export vault_port=$(kubectl get svc | grep vault-consul | awk '{print substr($5,6,5)}')
export vault_token=$(kubectl logs $vault_pod -c vault -n ${namespace} |grep "Root Token:" | awk  '{print $3}' | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})*)?m//g")
```

2. Create the dedicated back-end for your target tenant.
```
# specify target namespace
export tenant=xxxx
export header="X-Vault-Token:$vault_token"
curl -X POST -H $header -H "Content-Type:application/json" -d '{"type":"generic","description":"description","config":{"max_lease_ttl":"876000"}}' http://${icp_master_ip}:$vault_port/v1/sys/mounts/{tenant}
```

3. Populate the environment configuration data to Vault.
```
# specify target environment name
export envname=xxxx
# specify target keypath
export keypath=xxxx
# update ReplaceValueHere with real value
curl -X POST -H "$header" -d "{\"value\":\"ReplaceValueHere\"}" http://${icp_master_ip}:$vault_port/v1/{tenant}/{envname}/{keypath}
```

Note: Update the "xxxx" with the correct value.

Here are the samples of the relative key-value pair for mandatory parameters on Vault when deploying WebSphere Commerce Version 9 with "http://VaultIP:Port/v1/{tenant}/{envname}/":
```
domainName=<NameSpace>.svc.cluster.local
auth/dbHost=9.11x.24x.19x
auth/dbName=mall
auth/dbPassword=wcs1
auth/dbPassEncrypt=okFQawPB19Tkl1wKqQPDdAVtwz+mIgpJyqJQUuUWtyk=
auth/dbPort=50000
auth/dbUser=wcs
auth/dbType=Db2
auth/dbaPassEncrypt=u4h5LS/vJeSzsCfnt6NGfGHYPWK2fRq0+djkmB/iNWU=
auth/dbaUser=Db2inst1
auth/merchantKeyEncrypte=eZrWIdOyaDv5FCOTK32Uni288jgIHDv/P9wxhzKmHdiGZ+n8WJ+Ah56uPbfZ9yJWtjQlGczlmr6OgvArFHCgZQ==
auth/spiUserName=spiuser
auth/spiUserPwd=eNdqdvMAUGRUbiuqadvrQfMELjNScudSp5CBWQ8L6aw=
live/dbHost=9.11x.24x.19x
live/dbName=mall
live/dbPassword=wcs1
live/dbPassEncrypt=okFQawPB19Tkl1wKqQPDdAVtwz+mIgpJyqJQUuUWtyk=
live/dbPort=50001
live/dbUser=wcs
live/dbType=Db2
live/merchantKeyEncrypte=eZrWIdOyaDv5FCOTK32Uni288jgIHDv/P9wxhzKmHdiGZ+n8WJ+Ah56uPbfZ9yJWtjQlGczlmr6OgvArFHCgZQ==
live/spiUserName=spiuser
live/spiUserPwd=eNdqdvMAUGRUbiuqadvrQfMELjNScudSp5CBWQ8L6aw=
live/dbaPassEncrypt=u4h5LS/vJeSzsCfnt6NGfGHYPWK2fRq0+djkmB/iNWU=
live/dbaUser=Db2inst1
```

For more parameters, please see: [Key-value data structure in Consul/Vault](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.install.doc/refs/rigvaultmetadata.htm).

### Custom Encrypt Values ###
You may see some encrypted values in the Helm Chart values.yaml file. Here is the guide for how to encrypt them:

#### Encrypt MerchantKey and spiUserPwdAes ####
* Pull the WebSphere Commerce utility Docker image (commerce/ts-utils) to local.

* Launch the Utilities Docker container with the following command:
  Docker run –d –e LICENSE=accept <utilities Docker images>

* Log onto the Utilities Docker to run WebSphere Commerce Utilities with the following command:

  Docker exec –it <utilities container> /bin/bash

  /opt/WebSphere/CommerceServer90/bin/wcs_encrypt.sh <spiuser_password>

* Run wcs_encrypt.sh to encrypt Merchant key with KeyEncryptionKey.
  You can find the KeyEncryptionKey from file: /opt/WebSphere/AppServer/profiles/default/installedApps/localhost/ts.ear/xml/config/KeyEncryptionKey.xml.

  ```
  /opt/WebSphere/CommerceServer90/bin/wcs_encrypt.sh <merchantKey> <KeyEncryptionKey>
   For example:
  /opt/WebSphere/CommerceServer90/bin/wcs_encrypt.sh 1a1a1a1a1a1a1a1a2b2b2b2b2b2b2b2b 1234567890abcdef1234567890abcdef
  ```

#### Encrypt spiUserPwdBase64 ####
* To encrypt spiuser name and spiuser password with Base64, run the following command on Linux:
  echo –n “<spiuser_namename>:<spiuser_password>” | base64


## Storage
We recommend mount storage to search master and search repeater to persistent search index. Otherwise, search index will be stored inside of container will gone when container be killed.

* WebSphere Commerce V9 helm chart support auto create PVC and mount to search master or search repeater by enable "persistenVolumeClaim". But it just work with storage driver which
  support auto create PV ( e.g Glusterfs / vSphere / Ceph )

* WebSphere Commerce V9 helm chart support user to specify existed PVC for search master or search repeater by specify values in "searchAppRepeater.persistenVolumeClaim" or "searchAppMaster.persistenVolumeClaim"

* Specify existed PVC has high priority then enable auto create PVC by Helm when both of them be enabled.

* WebSphere Commerce V9 default container support to mount configMap in related namespace to expose OS variable by input the configMap name for "common.bindingConfigMap". See [WebSphere Commerce V9 Parameter Name]()

  > Note:

  1. We recommend naming the configMap with Tenat/Environment/EnvironmentType naming pattern
  2. The configMap must in the same namespace with target release
  3. The config map use "key=value" format

     ```
     key1=value1
     key2=value2
     ```

  4. Mount configMap is insecurity approach and should just for non-sensitive data.
  


## Installing the Chart
```
$ helm install --name=xxxx ./ibm-websphere-commerce -f ./< customized values.yaml > --namespace <namespace>
```

## Verifying the Chart
When you finish your configuration, you can run helm install command to install WebSphere Commerce Version 9, or use helm upgrade to update the environment.

When you install or update, WebSphere Commerce Version 9 Container startup must follow this sequence. The initContainer will automatically control it. The whole deploy process can take on average of 8-10 mins, depending on the capacity of the Kubernetes worker nod.

When you check the deployment status, the following values can be seen in the Status column:
```
– Running: This container is started.
– Init: 0/1: This container is pending on another container to start.
```
You may see the following values in the Ready column:
```
– 0/1: This container is started but the application is not yet ready.
– 1/1: This application is ready to use.
```

Run the following command to make sure there are no errors in the log file:
```
kubectl logs <pod_name> -f
```

## Uninstalling the Chart
To uninstall/delete the release deployment:
```
$ helm delete release-name --purge --tls
```

## Access Environment
By default, the Helm Chart uses the default value (tenant / env / envtype ). If you change those values, update your variable value to replace demoqaauth with following steps.

1. Check ingress server IP address.
```
kubectl get ingress -n <namespace>
```

2. Create the ingress server IP and hostname mapping on your server  by editing the  /etc/hosts file.

For auth enviornment:
```
<Ingress_IP>  store.demoqaauth.ibm.com cmc.demoqaauth.ibm.com org.demoqaauth.ibm.com accelerator.demoqaauth.ibm.com admin.demoqaauth.ibm.com tsapp.demoqaauth.ibm.com search.demoqaauth.ibm.com
```

For live environment:
```
<Ingress_IP>  store.demoqalive.ibm.com cmc.demoqalive.ibm.com org.demoqalive.ibm.com accelerator.demoqalive.ibm.com admin.demoqalive.ibm.com tsapp.demoqalive.ibm.com searchrepeater.demoqalive.ibm.com
```

Note: search.demoqaauth.ibm.com use to expose search master service.  searchrepeater.demoqalive.ibm.com use to expose search repeater sericve on live for trigger index replica.

3. Access the environment with following URLs:

      StoreFront:
      https://store.demoqaauth.ibm.com/wcs/shop/en/auroraesite

      Management Center (with default user wcsadmin/wcs1admin):
      https://cmc.demoqaauth.ibm.com/lobtools/cmc/ManagementCenter

      Organization Admin Console (with default user wcsadmin/wcs1admin):
      https://org.demoqaauth.ibm.com/webapp/wcs/orgadmin/servlet/ToolsLogon?XMLFile=buyerconsole.BuyAdminConsoleLogon

     Accelerator (with default user wcsadmin/wcs1admin):
      https://accelerator.demoqaauth.ibm.com/webapp/wcs/tools/servlet/ToolsLogon?XMLFile=common.mcLogon

     WebSphere Application Server Admin Console (with default user wcsadmin/wcs1admin):
      https://admin.demoqaauth.ibm.com/webapp/wcs/admin/servlet/ToolsLogon?XMLFile=adminconsole.AdminConsoleLogon

4. Trigger the Build Index with default master catalog ID ( default spisuer name is spiuser, default spiuser password is passw0rd, default masterCatalogID with sample data is 10001 ).

    curl -X POST -u spiuser:passw0rd https://tsapp.demoqaauth.ibm.com/wcs/resources/admin/index/dataImport/build?masterCatalogId=10001 -k

    You should get a response with a jobStatusId.

5. Check the Build Index Status ( default spisuer name is spiuser, default spiuser password is passw0rd )

    curl -X GET -u spiuser:passw0rd https://tsapp.demoqaauth.ibm.com/wcs/resources/admin/index/dataImport/status?jobStatusId=$1 -k

## Limitations
1. WebSphere Commerce Version 9 deploying with "EnvironmentParameter" mode is not supported through ICP catalog UI.
2. AMD64 is the only supported architecture.
3. Deploying Utilities is not supported.
4. Not support specify self-owne ingress secret before deployment. But can change it after ingress object be created.

## Documentation
* [Setting up a WebSphere Commerce Runtime environment with ICP](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.install.doc/concepts/cig_wcicpover.htm)
* [Deploying WebSphere Commerce Version 9 on Kubernetes](https://developer.ibm.com/customer-engagement/tutorials/deploy-websphere-commerce-version-9-kubernetes/)
* [WebSphere Commerce runtime environment overview](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.install.doc/refs/riginfrastructure.htm)
* [WebSphere Commerce DevOps Utilities Tool Chain](https://github.com/IBM/wc-devops-utilities)
