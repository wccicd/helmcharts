
# Breaking Changes


# What's new in Chart Version 1.0.3
* WebSphere Commerce Version 9.0.1.5 default Docker image
* Filebeat Version 1.0.1 Docker image
* Support deploy user extension component
* Support specify exist PVC for search master and search repeater
* Support podAntiAffinity between container from same compoennt
* Support specify nodeAffinity on label "wc-node-select-flag" with requiredDuringSchedulingIgnoredDuringExecution

# Fixes
* Fix db2 user expired.

# Prerequisites
1. ibm-websphere-commerce-vaultconsul is been deployed or you have deployed vault/consul manually
2. RBAC is created for non-default namespace. Otherwise, deploy on "default" namespace.
3. "Quick Deploy Model" requires that Vault/Consul is deployed with WebSphere Commerce Version 9 on the same namespace.
4. Vault URL and Docker image repository are configured for Helm Charts values.yaml.
5. The WebSphere Commerce Version 9 default Docker image can be pulled on Kubernetes worker node.
6. The WebSphere Commerce Version 9 default Docker image can be loaded into the Commerce namespace. If you deploy on non-commerce namespace, create imagePullSecrets, then specify the imagePullSecrets name in configuration view.
7. Optinoal: If use Vault store sensitive data, please populate data to Vault and create secrete object with Vault token for dedicated Vault backend

# Documentation


# Version History

|Chart | Date        | ICP Version Required | Image(s) Supported         | Details  |
|----- | ----------- | ------------------- | -------------------------- | -------------------------------------------------------------------|
|1.0.0 | Oct, 2018 | >= ICP 3.1   | Commerce V9 >=9.0.1 | This is the version for WebSphere Commerce Version 9 Helm Chart for ICP. |
|1.0.1 | Nov, 2018 | >= ICP 3.1   | Commerce V9 >=9.0.1 | This is the version for WebSphere Commerce Version 9 Helm Chart for ICP. |
|1.0.2 | Dec, 2018 | >= ICP 3.1   | Commerce V9 >=9.0.1 | This is the version for WebSphere Commerce Version 9 Helm Chart for ICP. |
|1.0.3 | JAN, 2019 | >= ICP 3.1   | Commerce V9 >=9.0.1 | This is the version for WebSphere Commerce Version 9 Helm Chart for ICP. |
